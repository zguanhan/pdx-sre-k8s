FROM python:3.8.12-alpine3.14 AS static_1
WORKDIR /app/
COPY . /app/
RUN apk add postgresql-libs &&                  \
    apk add --virtual .build-deps gcc musl-dev  \
        postgresql-dev &&                       \
    python3 -m pip install                      \
        Django==3.2.7                           \
        gunicorn==20.1.0                        \
        psycopg2-binary==2.9.1                  \
    &&                                          \
    apk --purge del .build-deps                 \
    && python3 manage.py collectstatic --no-input

FROM nginx:1.21.3-alpine
COPY --from=static_1 /app/static/ /app/static/
COPY ./nginx/* /etc/nginx/templates/

