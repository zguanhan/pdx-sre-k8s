# Install cri-o

[cri-o](https://cri-o.io) is a container runtime for k8s

Install:

```shell
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg lsb_release
```

```shell
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

```
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
```

# Install k8s

## Pre-install configuration

```shell
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF
sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
sudo sysctl --system
```

## Install k8s
```shell
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
```

```shell
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```

# K8s Cluster

Create a cluster configuration file:

```shell
cat <<EOF | tee kubeadm-config.yaml
kind: ClusterConfiguration
apiVersion: kubeadm.k8s.io/v1beta2
kubernetesVersion: $(kubeadm version -o short)
networking:
  podSubnet: "10.244.0.0/16"
---
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
cgroupDriver: systemd
---
kind: InitConfiguration
apiVersion: kubeadm.k8s.io/v1beta2
nodeRegistration:
  criSocket: "/var/run/crio/crio.sock"
  taints: []
EOF
```

Start a cluster:

```shell
sudo kubeadm init --config kubeadm-config.yaml
```

Allow users to run `kubectl`:

```shell
mkdir -p $HOME/.kube
sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Check the node is configured:

```shell
kubectl get nodes -o wide
```


# K8s Networking

A networking solution needs to be configured, or else the node might remain `NotReady`

## Antrea

Antrea is one of the many networking solutions for kubernetes.

Install cilium:

```shell
kubectl apply -f https://github.com/antrea-io/antrea/releases/download/v1.2.3/antrea.yml
```

Check its status:

```shell
kubectl get pods -A -o wide
```


# K8s Ingress

To allow external traffic into the cluster, we set up the nginx ingress controller.

Stop nginx (to avoid port conflicts):

```shell
sudo systemctl stop nginx
sudo systemctl disable nginx
```

Start nginx ingress:

```shell
kubectl apply -f ingress-nginx/deploy.yaml
```

Verify ingress is running:

```shell
kubectl get pods -n ingress-nginx -o wide
```
