# SRE Project

Deploy the project via kubernetes and leverage rolling update and high availibility!

## Set up

If you haven't:
  - install crio
  - install kubernetes
  - set up a kubernetes cluster
  - install antrea
  - install nginx-ingress

Then follow the instructions [here](docs/README.md).

## Project structure

| Directory      | Description                     |
|----------------|---------------------------------|
| deployments/   | Kubernetes manifests            |
| docs/          | Quick start for installing k8s  |
| ingress-nginx/ | Modified ingress controller     |
| monitoring/    | Filebeat & Metricbeat manifests |

The rest of the directories are the same with or without k8s.

## Deploy

### k8s ConfigMap

Modify `deployments/sre-config.yaml` under `data` field to configure your postgres user and postgre database. The postgres container and the django app will use this settings.

```shell
# apply sre-config
kubectl apply -f deployments/sre-config.yaml
```

### k8s Secret

Use the default secrets (only for demonstration purposes)

```shell
# apply default-sre-secret (don't do this in the production environment)
kubectl apply -f deployments/default-sre-secret.yaml
```

**In production environment, create the secrets via the command line (without the angle brackets):**

```shell
# Only do this if you don't already have a dot env file.
cp .env.example .env
```

Modify `.env` and fill in your credentials.

```shell
kubectl create secret generic sre-secret --from-env-file=.env
```

**Updating secrets in the production environment (after modifying .env):**

```shell
kubectl create secret generic sre-secret \
    --save-config --dry-run=client \
    -o yaml \
    --from-env-file=.env \ |
  kubectl apply -f -
```

### Postgres

Start the postgres database. The database will persist over container restarts, machine reboots, etc

```shell
sudo rm -rf /data/srepostgres/ && sudo mkdir -p /data/srepostgres/
kubectl apply -f deployments/sre-db.yaml
```

### Application and webserver

Deploy django app and nginx server:

```shell
kubectl apply -f deployments/sre-app.yaml
kubectl apply -f deployments/sre-nginx.yaml
```

### Nginx ingress

Modify `deployments/sre-ingress.yaml`:

```shell
kubectl apply -f deployments/sre-ingress.yaml
```

**If you choose to use TLS,**

You'll need to create a secret with the tls certificate and the private key:

```shell
kubectl create secret tls sre-tls --cert=path/to/cert/file --key=path/to/key/file
```

Be sure to change the two `host` fields in `deployments/sre-ingress-tls.yaml` to your domain name if you're using the tls version.

```shell
kubectl apply -f deployments/sre-ingress-tls.yaml
```
