# Monitoring

```shell
cp monitoring/.env.example monitoring/.env
```

Modify the dot env file and set the content to your Monitoring machine's IP and the elastic username/password.

Before applying the manifests, create the k8s secret first (ignore the warning):

```shell
kubectl create secret generic -n kube-system monitor-secret --from-env-file=monitoring/.env
# Or update the secret
#kubectl create secret generic -n kube-system monitor-secret --from-env-file=monitoring/.env -o yaml --dry-run=client --save-config | kubectl apply -f -
```

Spin up kube-state-metrics:

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/kube-state-metrics/release-2.2/examples/standard/service-account.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/kube-state-metrics/release-2.2/examples/standard/cluster-role.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/kube-state-metrics/release-2.2/examples/standard/cluster-role-binding.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/kube-state-metrics/release-2.2/examples/standard/deployment.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/kube-state-metrics/release-2.2/examples/standard/service.yaml
```

Finally, deploy filebeat and metricbeat:

```shell
kubectl apply -f monitoring/filebeat.yaml
kubectl apply -f monitoring/metricbeat.yaml
```
